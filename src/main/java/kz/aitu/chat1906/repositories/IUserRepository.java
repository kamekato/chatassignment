package kz.aitu.chat1906.repositories;

import kz.aitu.chat1906.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User,Long> {
}
