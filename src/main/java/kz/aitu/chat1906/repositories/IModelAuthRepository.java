package kz.aitu.chat1906.repositories;

import kz.aitu.chat1906.models.ModelAuth;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IModelAuthRepository extends JpaRepository<ModelAuth, Long> {
    List<ModelAuth> findModelAuth (String login, String password);

}
