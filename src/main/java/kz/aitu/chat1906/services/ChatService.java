package kz.aitu.chat1906.services;

import kz.aitu.chat1906.models.Chat;
import kz.aitu.chat1906.models.Participant;
import kz.aitu.chat1906.repositories.IChatRepository;
import kz.aitu.chat1906.repositories.IParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ChatService {
    private static IParticipantRepository iParticipantRepository;
    private static IChatRepository iChatRepository;
    public static List<Chat> userChats(Long userId){
        List<Participant> participants = iParticipantRepository.findUsersByUserId(userId);
        List<Chat> chats = new LinkedList<>();
        for(Participant p : participants){
            Optional<Chat> opChat = iChatRepository.findById(p.getChatId());
            if(opChat.isPresent()){
                chats.add(opChat.get());
            }
        }
        return chats;
    }
}

