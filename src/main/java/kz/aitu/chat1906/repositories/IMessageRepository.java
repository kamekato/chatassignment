package kz.aitu.chat1906.repositories;

import kz.aitu.chat1906.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IMessageRepository extends JpaRepository<Message, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM message WHERE chat_id = ?1 ORDER BY id DESC LIMIT 10")
    List<Message> lastTenMessagesInChat(Long chatId);

    @Query(nativeQuery = true, value = "SELECT * FROM message WHERE user_id = ?1 ORDER BY id DESC LIMIT 10")
    List<Message> lastTenMessagesOfUser(Long userId);


    List<Message> findByChatIdAndDeliveredFalse(Long chatId);
}
