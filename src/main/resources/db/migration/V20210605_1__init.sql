drop table if exists model_auth;
create table model_auth(
    id serial,
    login varchar(255),
    password varchar(255)
    last_login_timestamp bigint,
    user_id bigint,
    token varchar(255)
);