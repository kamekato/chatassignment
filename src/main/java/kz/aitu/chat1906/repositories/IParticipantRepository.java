package kz.aitu.chat1906.repositories;

import kz.aitu.chat1906.models.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IParticipantRepository extends JpaRepository<Participant,Long> {
    List<Participant> findUsersByChatId(Long chatId);
    List<Participant> findUsersByUserId(Long userId);

}
