package kz.aitu.chat1906.controllers;

import kz.aitu.chat1906.services.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/chat")
@AllArgsConstructor
public class ChatController {
    private ChatService chatService;

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> userChats(@PathVariable Long userId){
        return ResponseEntity.ok(ChatService.userChats(userId));
    }
}
