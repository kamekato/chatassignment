package kz.aitu.chat1906.services;

import kz.aitu.chat1906.models.Message;
import kz.aitu.chat1906.repositories.IMessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {
    private IMessageRepository messageRepository;

    public List<Message> lastTenMessagesInChat(Long chatId){
        return changeFields(messageRepository.lastTenMessagesInChat(chatId));
    }

    public List<Message> lastTenMessagesOfUser(Long userId){
        return changeFields(messageRepository.lastTenMessagesOfUser(userId));
    }

    private List<Message> changeFields(List<Message> messages){
        messages.forEach(message -> {
            message.setDelivered(true);
            message.setDeliveredTimestamp(new Date().getTime());
        });
        return messages;
    }

    public void add(Message message) {
        Date date = new Date();
        message.setCreatedTimestamp(date.getTime());
        messageRepository.save(message);
    }

    public void edit(Message message) {
        Date date = new Date();
        message = messageRepository.findById(message.getId()).get();
        message.setUpdatedTimestamp(date.getTime());
        messageRepository.save(message);
    }

    public void delete(Long id){
        messageRepository.deleteById(id);
    }

    public Message setRead(Long messageId){
        Message message = messageRepository.findById(messageId).get();
        message.setRead(true);
        message.setReadTimestamp(new Date().getTime());
        return messageRepository.save(message);
    }

    public List<Message> getNotDeliveredMessages(Long chatId){
        return messageRepository.findByChatIdAndDeliveredFalse(chatId);
    }
}