package kz.aitu.chat1906.services;

import kz.aitu.chat1906.models.Participant;
import kz.aitu.chat1906.models.User;
import kz.aitu.chat1906.repositories.IChatRepository;
import kz.aitu.chat1906.repositories.IParticipantRepository;
import kz.aitu.chat1906.repositories.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {
    private IUserRepository iUserRepository;
    private IChatRepository iChatRepository;
    private IParticipantRepository iParticipantRepository;

    public List<User> usersInChat(Long chatId){
        List<Participant> participants = iParticipantRepository.findUsersByChatId(chatId);
        List<User> users = new LinkedList<>();
        for(Participant p : participants){
            Optional<User> opUser = iUserRepository.findById(p.getUserId());
            if(opUser.isPresent()){
                users.add(opUser.get());
            }
        }
        return users;
    }
}

