package kz.aitu.chat1906.controllers;

import kz.aitu.chat1906.models.ModelAuth;
import kz.aitu.chat1906.services.ModelAuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/modelAuth")
@AllArgsConstructor

public class ModelAuthController {
    private final ModelAuthService modelAuthService;

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> findModelAuth(@PathVariable String login, String password){
        return ResponseEntity.ok(ModelAuthService.findModelAuth(login, password));
    }
}
