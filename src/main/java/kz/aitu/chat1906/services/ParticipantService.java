package kz.aitu.chat1906.services;

import kz.aitu.chat1906.repositories.IParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ParticipantService {
    private IParticipantRepository participantRepository;

}
