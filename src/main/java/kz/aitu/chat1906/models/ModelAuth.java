package kz.aitu.chat1906.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "model_auth")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class ModelAuth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String login;
    private String password;
    @Column(name = "last_login_timestamp")
    private Long lastLoginTimestamp;
    @Column(name = "user_id")
    private Long userId;
    private String token;
}
