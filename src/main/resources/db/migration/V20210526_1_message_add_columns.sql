alter table message add column is_read boolean;
alter table message add column read_timestamp bigint;
alter table message add column is_delivered  boolean;
alter table message add column delivered_timestamp bigint;