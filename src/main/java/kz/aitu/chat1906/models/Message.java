package kz.aitu.chat1906.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "message")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "chat_id")
    private Long chatId;

    private String text;

    @Column(name = "created_timestamp")
    private Long createdTimestamp;

    @Column(name = "updated_timestamp")
    private Long updatedTimestamp;

    @Column(name = "is_read")
    private Boolean read;

    @Column(name = "read_timestamp")
    private Long readTimestamp;

    @Column(name = "is_delivered")
    private Boolean delivered;

    @Column(name = "delivered_timestamp")
    private Long deliveredTimestamp;

    private String status;
}