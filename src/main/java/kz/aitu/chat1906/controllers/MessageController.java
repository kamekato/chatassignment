package kz.aitu.chat1906.controllers;

import kz.aitu.chat1906.models.Message;
import kz.aitu.chat1906.services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/message")
@AllArgsConstructor
public class MessageController {

    private MessageService messageService;


    @GetMapping("/chat/{chatId}")
    public ResponseEntity<?> lasTenMessagesInChat(@PathVariable Long chatId){
        return ResponseEntity.ok(messageService.lastTenMessagesInChat(chatId));
    }


    @GetMapping("/user/{userId}")
    public ResponseEntity<?> lastTenMessagesOfUser(@PathVariable Long userId){
        return ResponseEntity.ok(messageService.lastTenMessagesOfUser(userId));
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Message message){
        messageService.add(message);
        return ResponseEntity.ok("Added");
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Message message){
        messageService.edit(message);
        return ResponseEntity.ok("Edited");
    }


    @DeleteMapping("/{messageId}")
    public ResponseEntity<?> delete(@PathVariable Long messageId){
        messageService.delete(messageId);
        return ResponseEntity.ok("Deleted");
    }

    @PostMapping("/message/{messageId}/setRead")
    public ResponseEntity<?> setRead(@PathVariable Long messageId){
        return ResponseEntity.ok(messageService.setRead(messageId));
    }

    @GetMapping("/notDelivered/chat/{chatId}")
    public ResponseEntity<?> getNotDeliveredMessages(@PathVariable Long chatId){
        return ResponseEntity.ok(messageService.getNotDeliveredMessages(chatId));
    }

}