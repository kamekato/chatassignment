package kz.aitu.chat1906.controllers;

import kz.aitu.chat1906.services.MessageService;
import kz.aitu.chat1906.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
@AllArgsConstructor
public class UserController {
    private MessageService messageService;
    private UserService userService;

    @GetMapping("/chat/{chatId}")
    public ResponseEntity<?> usersInChat(@PathVariable Long chatId){
        return ResponseEntity.ok(userService.usersInChat(chatId));
    }
}


