package kz.aitu.chat1906.services;

import kz.aitu.chat1906.models.ModelAuth;
import kz.aitu.chat1906.repositories.IModelAuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ModelAuthService {
    private static IModelAuthRepository iModelAuthRepository;

    public static String findModelAuth(String login, String password) {
        ModelAuth modelAuth = (ModelAuth) iModelAuthRepository.findModelAuth(login, password);

        if (modelAuth != null) {
            modelAuth.setLastLoginTimestamp(new Date().getTime());
            modelAuth.setToken(UUID.randomUUID().toString());
            iModelAuthRepository.save(modelAuth);
            return "Successful";
        }
        else{
            return "not Successful";
        }
    }

}
